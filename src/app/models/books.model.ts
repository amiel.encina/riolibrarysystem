import { IMAGES_URL } from 'src/environments/environment';

export class Book {
  id: number;
  image_url: String;
  title: String;
  author: String;
  section: String;
  genre: String;
  status: String;

  get image() {
    if(this.image_url === '') {
      this.image_url = 'src/assets/book_cover.jpg'
    }
    return `${IMAGES_URL}/${this.image_url}`;
  }
}