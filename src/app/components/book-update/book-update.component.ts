import { Component, OnInit } from '@angular/core';
import { BookService } from 'src/app/services/book.service';
import { Book } from 'src/app/models/books.model';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Genres } from 'src/app/Genres';
import { Sections } from 'src/app/Sections';
@Component({
  selector: 'app-book-update',
  templateUrl: './book-update.component.html',
  styleUrls: ['./book-update.component.scss']
})
export class BookUpdateComponent implements OnInit {

  books: Book;
  genres;
  sections;

  constructor(
    private bookService: BookService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.genres = Genres;
    this.sections = Sections;
    this.bookService.getBookById(this.route.snapshot.paramMap.get('id')).subscribe(res => {
      this.books = res[0];
    }, err => {
      console.log(err);
    })
  }

  updateBook(f: NgForm) {
    this.bookService.updateBook(this.route.snapshot.paramMap.get('id'), f.value).subscribe(res => {
      console.log(res);
    }, err => {
      console.log(err);
    })
  }

}
