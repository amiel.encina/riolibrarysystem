import { Component, OnInit } from '@angular/core';
import { Book } from '../../models/books.model';
import { BookService } from 'src/app/services/book.service';
import { Filters } from '../../Filters';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'my-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {
  books: Array<Book>;
  booksList: Array<Book>;
  searchText: string;
  filters;
  isHidden: boolean = true;
  toggleMark: boolean = true;
  markButtonText = 'Mark as Borrowed';
  bookFilters: any = null;
  checked: boolean = false;

  constructor(
    private bookService: BookService
  ) { }

  ngOnInit() {
    this.filters = Filters;
    this.getAllBooks();
  }

  getAllBooks() {
    this.bookService.getBooks().subscribe(res => {
      this.booksList = res;
      this.books = this.booksList.filter(book => book.status !== 'Borrowed');
    }, err => {
      console.log(err);
    });
  }

  toggleFilters() {
    this.isHidden = !this.isHidden;
  }

  updateStatus(book) {
    this.bookService.updateBook(book.id, {status: 'Borrowed'}).subscribe(res => {
      this.getAllBooks();
    }, err => {
      console.log(err);
    })
  }

  filterBooks(searchText, options) {
    console.log(options);
    if (options) {
      this.books = this.books.filter(book => {
        let filteredBooks;
        options.forEach((option, index) => {
          filteredBooks = book[option] === (searchText);
        });

        return filteredBooks;
      });
    }
    if (searchText === null || searchText === undefined || searchText === '') {
      this.books = this.booksList;
    } else if (searchText !== null || searchText !== undefined) {
      this.books = this.books.filter(book => Object.values(book).includes(searchText));
    }
  }

  // toggleFilterBoxes(value) {
  //   this.checked = !this.checked;
  //   if(this.checked) {
  //     this.bookFilters.push(value);
  //   } else {
  //     this.bookFilters = this.bookFilters.filter(filterValue => filterValue !== value);
  //   }
  //   console.log(this.bookFilters);
  // }

  deleteBook(id) {
    this.bookService.deleteBook(id).subscribe(res => {
      console.log(res);
    }, err => {
      console.log(err);
    })
  }

}
