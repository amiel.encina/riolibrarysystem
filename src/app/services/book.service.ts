import { Injectable } from '@angular/core';
import { BOOKS_URL, BASE_URL } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from '../models/books.model';


@Injectable({
  providedIn: 'root'
})
export class BookService {
  baseUrl = BOOKS_URL;
  httpOptions = {
    headers: new HttpHeaders({
      'content-type': 'application/json',
      'Accept': '*/*'
    })
  };
  constructor(
    private http: HttpClient
  ) { }


  getBooks = (): Observable<Book[]> => {
    return this.http.get<Book[]>(this.baseUrl);
  }

  getBookById = (id): Observable<Book> => {
    return this.http.get<Book>(`${this.baseUrl}/${id}`);
  }

  addBook = (bookData): Observable<Book> => {
    return this.http.post<Book>(this.baseUrl, bookData, this.httpOptions);
  }

  updateBook = (id, bookData): Observable<Book> => {
    return this.http.put<Book>(`${this.baseUrl}/${id}`, bookData, this.httpOptions);
  }

  deleteBook = (id): Observable<Book> => {
    return this.http.delete<Book>(`${this.baseUrl}/${id}`, this.httpOptions);
  }
}
