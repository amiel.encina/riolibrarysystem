export const Filters = [
  {
    name: 'Book Title',
    value: 'title'
  },
  {
    name: 'Author',
    value: 'author'
  },
  {
    name: 'Genre',
    value: 'genre'
  },
  {
    name: 'Section',
    value: 'section'
  },
]