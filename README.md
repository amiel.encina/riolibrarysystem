# Rio's Library System Client

This project is for the UI/UX of the web app.

### Installing

Open the terminal and type: 
```
  npm i
```

## Deployment

Open the terminal then type:
```
  ng serve -o
```

## Built With

* [Angular 8](https://angular.io/) - Javascript Framework used for developing the Front-end tasks.



## Author

* **Amiel Encina** - *Developer*

